import matplotlib.pyplot as plt
from sklearn.metrics import adjusted_rand_score
from numpy.linalg import norm
import numpy as np
import pandas as pd
import math


class FuzzyCMeans:
    k = None
    m = None
    d = None
    N = None
    X = None
    upper_bound = 10
    W = None
    center = None
    y = None

    def __init__(self, k=2, m=1.0, upper_bound=10):
        self.k = k
        self.m = m
        self.upper_bound = upper_bound
        print("FuzzyCMeans: k: {}, m: {}, upper_bound: {}".format(k,m,upper_bound))

    def fit(self,X):
        self.X = np.asarray(X)
        self.N = len(X)
        self.d = len(X[0])
        self.W = np.random.rand(self.N, self.k)

        for r in range(self.upper_bound):
            self.center = []

            for i in range(self.k):
                self.center.append(self.__calculateCenter(i))

            self.center = np.asarray(self.center)

            for i in range(self.N):
                for j in range(self.k):
                    den = 0

                    if(self.m == 1):
                        factor = np.inf
                    else:
                        factor = 2 / (self.m - 1.0)

                    for k in range(len(self.center)):
                        den += (self.__distance(self.X[i],self.center[j]) / self.__distance(self.X[i], self.center[k]))**factor

                    self.W[i, j] = 1.0 / den

    def __distance(self,p1,p2):
        d = 0
        for i in range(len(p1)):
            d += (p1[i]-p2[i])**2
        
        d=math.sqrt(d)
        return (d)

    def __calculateCenter(self, c):
        num = 0
        den = 0

        for i in range(self.N):
            num += (self.W[i, c]**self.m) * self.X[i, :]
            den += self.W[i, c]**self.m

        return num / den

    def predict(self):
        self.y = np.argmin(self.W, axis=1)
        return self.y


if __name__ == "__main__":
    dataset = pd.read_csv("cleanData/0_datos.csv", sep=",", header=None)
    X = dataset.loc[:, :1]
    y = np.asarray(dataset.loc[:, 2])
    fcm = FuzzyCMeans(3, 0.8, 20)
    fcm.fit(X)
    y_pred = fcm.predict()

    print("ARI: ", adjusted_rand_score(y, y_pred))

    X = np.asarray(X)

    plt.title("FuzzyCmeans")
    plt.scatter(X[:, 0], X[:, 1], c=y_pred)
    plt.show()
    plt.title("Real")
    plt.scatter(X[:, 0], X[:, 1], c=y)
    plt.show()
