from sklearn import cluster, datasets
from sklearn.metrics.cluster import adjusted_rand_score, adjusted_mutual_info_score
from sklearn import metrics
from FuzzyCMeans import *
from sklearn import datasets
import numpy as np
import pandas as pd

def k_means(k, X, y):
    k_means = cluster.KMeans(n_clusters=k, random_state=0)
    k_means.fit(X)
    y_pred = k_means.predict(X)

    #Adjusted Rand Score
    ARI = adjusted_rand_score(y, y_pred)
    #print("Adjusted Rand Index", ARI)
    #Mutual Information
    MI = adjusted_mutual_info_score(y, y_pred)
    #print("Mutual Information", MI)
    
    getPlot(X,"KMeans",y_pred)
    return [ARI, MI]


def dbscan(X, y,e,m):
    model = cluster.DBSCAN(eps=e, min_samples=m)
    model.fit_predict(X)
    y_pred = model.labels_.astype(np.int)
    #Adjusted Rand Score
    ARI = adjusted_rand_score(y, y_pred)
    #print("Adjusted Rand Index", ARI)
    #Mutual Information
    MI = adjusted_mutual_info_score(y, y_pred)
    #print("Mutual Information", MI)
    getPlot(X,"DBSCAN",y_pred)

    return [ARI, MI]

def agglomerative(k, X, y):
    model = cluster.AgglomerativeClustering(n_clusters=k)
    model = model.fit(X)
    y_pred = model.labels_.astype(np.int)
    #Adjusted Rand Score
    ARI = adjusted_rand_score(y, y_pred)
    #print("Adjusted Rand Index", ARI)
    #Mutual Information
    MI = adjusted_mutual_info_score(y, y_pred)
    #print("Mutual Information", MI)
    getPlot(X, "Agglomerative", y_pred)
    return [ARI, MI]

def fCMeans(k, X, y, m):
    model = FuzzyCMeans(k, m)
    model.fit(X)
    y_pred = model.predict()
    #Adjusted Rand Score
    ARI = adjusted_rand_score(y, y_pred)
    #print("Adjusted Rand Index", ARI)
    #Mutual Information
    MI = adjusted_mutual_info_score(y, y_pred)
    #print("Mutual Information", MI)
    getPlot(X, "FuzzyCMeans", y_pred)
    return [ARI, MI]

def getPlot(X,title,y_pred):
    X = np.asarray(X)
    plt.title(title)
    plt.scatter(X[:, 0], X[:, 1], c=y_pred)
    plt.savefig('plots_part2/'+title+'_circules.png')
    plt.show()
    

def getDataFactory(make, n_samples, noise):
    if 'circules' in make:
        return datasets.make_circles(n_samples=n_samples, factor=.5, noise=noise)
    elif 'moons' in make:
        return datasets.make_moons(n_samples=n_samples, noise=noise)
    else:
        print("DataSet no encontrado")

if __name__ == "__main__":
    n_samples = 200
    noise = 0.05
    # circules o moons
    make = "circules"
    data = getDataFactory(make, n_samples, noise)

    #print(data)
    k = 2
    #print(data.head())
    #print(len(data.columns))
    X = data[0]
    y = data[1]

    getPlot(X,"Noise Moons",y)
    #print(y)
    #print(X)
    KM_ARI, KM_MI = k_means(k, X, y)
    print("K_MEANS, ARI: {}, MI:{}".format(KM_ARI, KM_MI))

    DB_ARI, DB_MI = dbscan(X, y, 0.55, 40)
    print("DBSCAN, ARI: {}, MI:{}".format(DB_ARI, DB_MI))

    AG_ARI, AG_MI = agglomerative(k, X, y)
    print("Agglomerativo, ARI: {}, MI:{}".format(AG_ARI, AG_MI))

    FCM_ARI, FCM_MI = fCMeans(k, X, y, 1.2)
    print("FuzzyCMeans, ARI: {}, MI:{}".format(FCM_ARI, FCM_MI))
