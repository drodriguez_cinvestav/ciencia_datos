import numpy as np
import math
import random
from sklearn import datasets
from itertools import combinations
from sklearn import metrics
import matplotlib.pyplot as plt
from sklearn.cluster import DBSCAN
from sklearn.cluster import AgglomerativeClustering
from sklearn.metrics.cluster import normalized_mutual_info_score
from sklearn.cluster import KMeans
from FuzzyCMeans import *
from sklearn.metrics.cluster import adjusted_rand_score, adjusted_mutual_info_score

def getDataFactory(make, n_samples, noise=0.05):
    if 'circules' in make:
        return datasets.make_circles(n_samples=n_samples, factor=.5, noise=noise)
    elif 'moons' in make:
        return datasets.make_moons(n_samples=n_samples, noise=noise)
    elif 'blobs' in make:
        return datasets.make_blobs(n_samples=n_samples, centers=2, random_state=random.randint(0, 9), cluster_std=1)
    else:
        print("DataSet no encontrado")


#Esta funcion obtiene el dataset y lo normaliza entre 0 y 1
def getData(make, n_samples, noise):   #Numero de Datos que se tomaran para generar el DataSet
    dataSet = getDataFactory(make,n_samples,noise)

    data = dataSet[0]
    target = dataSet[1]

    #Se normaizan los datos entre 0 y 1
    for i in range(len(data[0])):
        mini = min(data[0:n_samples, i])
        maxi = max(data[0:n_samples, i])
        data[0:n_samples, i] = (data[0:n_samples, i]-mini)/(maxi-mini)
    return data, target

#Aqui se tiene la funcion para calcular la distancia eluclidiana de 2 puntos
def distancia(p1,p2):
  suma=0
  for i in range(len(p1)):
    suma += (p1[i]-p2[i])**2
  return math.sqrt(suma)

#Aqui se tiene una funcion para generar individuos aleatoreamente
def getIndividuo(dB,KmaxB):       #d, dimenciones del cluster,    kmax, cantidad maxima de clusters del cromosoma
  genes=[]
  for k in range(KmaxB):
    cluster = [np.random.rand()]   #Activacion
    centro=[]
    for j in range(dB):         #Posicion del centroide propuesto
        centro.append(np.random.rand())
      
    cluster.append(centro)
    genes.append(cluster)
  genes[0][0]=0.7
  genes[1][0]=0.7
  return genes

#Aqui se tiene una funcion que cuenta los elementos que tiene cada cluster
def ConteoZ(CromD,DataD):

  NumClusD = len(CromD) 
  num=[]  #Vector con la cuenta de elementos que pertenecen al CLUSTER
  for i in range(NumClusD): num.append(0)

  #Identifico los centroides inactivos del cromosoma con un -1
  estados=[]
  for i in range(NumClusD): 
    if(CromD[i][0]>0.5): estados.append(i)
    else: estados.append(-1)

  for i in range(len(DataD)):
    rev=[] #Calculo las distancias de un dato a todos los cluster activados
    pos=-1
    for j in range(NumClusD):
      if(estados[j]!=-1):
        rev.append(distancia(DataD[i],CromD[j][1]))
        #Aqui voy guardando el numero de cluster con la menor distancia (pos)
        if(pos==-1): pos=j
        if(rev[j]<rev[pos]): pos=j
      else: 
        rev.append(-1)
        num[j]=-1
    num[pos]=num[pos]+1
  #Num tiene un vector con los elementos que pertenecen a cada cluster
  return num

#Aqui se tiene la funcion que verifica que el cromosoma sea funcional. 2 clustes activos, minimo 2 elementos por cluster
def Verificar(CromE,DataE):
  DimencionE=len(CromE[0][1]) #Dimencion que tienen los puntos del dataset
  NumClusE= len(CromE) #Numero de cluster que tiene el cromosoma   

  #Identifico los cromosomas activos y no activos en 2 vectores
  actE=[]
  desE=[]
  for i in range(NumClusE):
    if(CromE[i][0]>0.5):
      actE.append(i)
    else:
      desE.append(i)

  #Cuento la cantidad de Cromosomas activos, si es menor que 2 activo por lo mucho 2 cromosomas 
  if(len(actE)==0):
    CromE[desE[0]][0]=0.6
    CromE[desE[1]][0]=0.6
  if(len(actE)==1):
    CromE[desE[0]][0]=0.6

  #Cuento la cantida de elementos que tiene cada cluster y alzo una bandera para indicar si existe un cluster con menos de 2 elementos
  Cont=ConteoZ(CromE,DataE)
  flag=0
  for i in range(len(Cont)):
    if(Cont[i]==0 or Cont[i]==1): flag =1

  #Aqui libero el cromosoma hasta que cumpla que todos sus cluster tengan por lo menos 2 elmentos
  cnt=0
  while(flag!=0 and cnt<30):
    numclu=random.randint(0,NumClusE-1)
    numdim=random.randint(0,DimencionE-1)
    CromE[numclu][1][numdim]=random.random()
    flag=0
    Cont=ConteoZ(CromE,DataE)    
    for i in range(len(Cont)):
      if(Cont[i]==0 or Cont[i]==1): flag =1
    if(cnt==29):
      print("Las caracterisitacas del clustering complica que cada cluster tenga por lo menos 2 elementos")
      CromE=getIndividuo(DimencionE,NumClusE)
      cnt=0
    cnt=cnt+1
 
#Aqui se programa el indice DBINDEX 
def DBIndex(CromF,DataF):

    Verificar(CromF,DataF)
    NumClusF=len(CromF)   #Cantidad de clusters que tiene el cromosoma

    s=[]    #Este vector contendra la cohesion de cada cluster 
    num=[]    #Este vector contendra el total de elementos que pertenecen al cluster
    for i in range(len(CromF)): s.append(0), num.append(0)    #Inicializo los vectores 

    #Identifico el cluster mas cercano que tiene cada dato, lo sumo y lo cuento
    for j in range(len(DataF)):
        mind=-1.  #Menor distancia actual dato-cluster
        posF=-1     #Numero del cluster mas cercano
        for i in range(NumClusF):
            if(CromF[i][0]>0.5):
                auxF=distancia(CromF[i][1],DataF[j])
                if(mind==-1):
                    mind=auxF
                    posF=i
                if(auxF<mind):
                    mind=auxF
                    posF=i
        s[posF]=s[posF]+mind
        num[posF]=num[posF]+1  
    
    #Calculo la cohesion de cada cluster (NINGUN CLUSTER QUE ESTA ACTIVO TENDRA MENOS DE 2 ELEMNOTS POR QUE ESTOS INDIVIUOS SE TIENE QUE VERIFICAR ANTES DE PASAR POR ESTA FUNCION)
    for i in range(NumClusF):
        if(s[i]!=0):s[i]=s[i]/num[i]

    #Calculo el valor de R para cada cluster segun la ecuacion 9 del articulo
    R=[]
    #Identifico los clusters que estan activos
    ActF=[]
    for i in range(NumClusF):
        if(CromF[i][0]>0.5):
            ActF.append(i)
            R.append(0)

    #Obtengo todas las poibles parejas que pueden formar los cluster y calculo la R que pueden formar
    par=combinations(ActF,2)
    par=list(par)
    r=[]
    for i in range(len(par)):
        auxF=(s[par[i][0]]+s[par[i][1]])/(distancia(CromF[par[i][0]][1],CromF[par[i][1]][1]))
        r.append(auxF)

    #Para cada cluster activo identifico la R mas grande a la que pertenece
    for i in range(len(ActF)):
        for j in range(len(par)):
            if(par[j][0]==ActF[i] or par[j][1]==ActF[i]): 
                if(R[i]<r[j]): R[i]=r[j]

    #Se regresa la suma maxima de las distancias encontradas
    RT=0
    for i in range(len(R)): RT=RT+R[i]

    return RT/len(R)


#Esta es una funcion que copia completamente un cromosoma
def Copy(CromUNO):
    nuevo=[]
    for i in range(len(CromUNO)):
        crom=[]
        Act=CromUNO[i][0]
        Clus=[]
        for j in range(len(CromUNO[0][1])):
            Clus.append(CromUNO[i][1][j])
        crom.append(Act)
        crom.append(Clus)
        nuevo.append(crom)
    return nuevo

#Esta funcion realiza la cruza aleatorea entre los elementos y el Best
def Cruza(CromH,BestH):
    if(random.random()>0.5):
        pos=random.randint(0,len(CromH)-1)
        for i in range(len(CromH)):
            if(pos>=i): CromH[i][0]=BestH[i][0]
        
        pos=random.randint(0,len(CromH[0][1])-1)
        for i in range(len(CromH)):
            for j in range(len(CromH[0][1])):
                if(pos>=j): CromH[i][1][j]=BestH[i][1][j]
                
    else:
        pos=random.randint(0,len(CromH)-1)
        for i in range(len(CromH)):
            if(pos<=i): CromH[i][0]=BestH[i][0]
        
        pos=random.randint(0,len(CromH[0][1])-1)
        for i in range(len(CromH)):
            for j in range(len(CromH[0][1])):
                if(pos<=j): CromH[i][1][j]=BestH[i][1][j]     

def Mutacion(CromG,Grado):
    pos=random.randint(0,len(CromG)-1)
    CromG[pos][0]=CromG[pos][0]+((random.random()*2)-1)*0.01
    
    for i in range(len(CromG)):
        pos=random.randint(0,len(CromG[0][1])-1)
        CromG[i][1][pos]= CromG[i][1][pos]+((random.random()*2)-1)*0.01*Grado

       
#Esta funcion etiqueta los elementos del data set
def Etiquetador(CentrosI,DataI):
  etiquetas=[]
  for i in range(len(DataI)):
    disI=-1     
    tar=-1
    for j in range(len(CentrosI)):
      auxI=distancia(DataI[i],CentrosI[j])
      if(disI==-1):
        disI=auxI
        tar=j
      if(auxI<disI):
        disI=auxI
        tar=j
    etiquetas.append(tar)
  
  return etiquetas

#Esta funcion sustrae solo las posiciones de los centroides activos del cromosoma recibido
def GetCentros(Cromk):
    Solucion=[]
    #Reviso todos los centros activos y los junto en una arreglo
    for i in range(len(Cromk)):
      if(Cromk[i][0]>0.5):
        Solucion.append(Cromk[i][1])
    return Solucion

#Esta funcion grafica los elementos que pertenecen a cada cluster segun sus etiquetas
def getPlot(DataX,TargetX,title):
    
    etiq=[0]
    for i in range(len(TargetX)):
        flag=0
        for j in range(len(etiq)):
            if(etiq[j]==TargetX[i]): flag=1
        if(flag==0): etiq.append(TargetX[i])

    clus=[]
    for i in range(len(etiq)): clus.append([])
    
    for i in range(len(DataX)):
        for j in range(len(etiq)):
            if(TargetX[i]==etiq[j]): clus[j].append(DataX[i])
    
    for i in range(len(clus)):
        x=[]
        y=[]
        for j in range(len(clus[i])):
            x.append(clus[i][j][0])
            y.append(clus[i][j][1])
        plt.title(title)
        plt.plot(x,y,'o',linewidth=3,color=(random.random(),random.random(),random.random()))
    plt.savefig('plots_part3/'+title)    
    plt.show()
    

#Esta es la funcion ACDE usando el indicador DB index
def ACDEDB(NumClusMAX,DataZ,Generaciones):
    Dimencion=len(DataZ[0])
    num_poblacion=4*Dimencion
    #print("Num de poblacion:",num_poblacion)
    #Inicializo una poblacion 
    Poblacion=[]
    newPoblacion=[]
    Fitness=[]
    Best=[]
    for i in range(num_poblacion):
        Poblacion.append(getIndividuo(Dimencion,NumClusMAX))
        newPoblacion.append(getIndividuo(Dimencion,NumClusMAX))
        Fitness.append(DBIndex(Poblacion[i],DataZ))
    
    for gen in range(Generaciones):
        #Identifoco el mejor elemento de la poblacion y lo pongo en Best
        mejor=0
        for i in range(num_poblacion):
            Fitness[i]=DBIndex(Poblacion[i],DataZ)
            if(Fitness[mejor]>Fitness[i]): mejor=i
        Best=Copy(Poblacion[mejor])
        
        #Realizo la seleccion de los individuso por Torneo
        for i in range(num_poblacion):
            prim=random.randint(0,num_poblacion-1)
            segu=random.randint(0,num_poblacion-1)
            mov=prim
            if(Fitness[prim]>Fitness[segu]): mov=segu
            newPoblacion[i]=Copy(Poblacion[mov])
        
        #Realizo la cruza de la nueva porblacion con el best y la verifico 
        for i in range(num_poblacion):
            Cruza(newPoblacion[i],Best)
            Verificar(newPoblacion[i],DataZ)
        
        #Realizo la mutacion de la poblacion 
        for i in range(num_poblacion):
            Mutacion(newPoblacion[i],i)
            Verificar(newPoblacion[i],DataZ)
            #print(ConteoZ(newPoblacion[i],DataZ))
        
        #La nueva poblacion es la poblacion de la sigueinte generacion y agrego el Best aleatoreamente
        for i in range(num_poblacion):
            Poblacion[i]=Copy(newPoblacion[i])
        pos=random.randint(0,num_poblacion-1)
        Poblacion[pos]=Copy(Best)
    return Best

#Se adquieren los datos   1: noisycircles  2: noisymoons 3 coriculitos
if __name__ == "__main__":
  n_samples = 200
  noise = 0.05
  # moons, circules, blobs
  make = 'moons'

  data,target=getData(make, n_samples,noise)
  getPlot(data,target, "DataSet Original ("+make+")")

  #Aqui se utiliza DBINDEX
  individuo = ACDEDB(2,data,10)
  centros = GetCentros(individuo)
  y_train=Etiquetador(centros,data)
  getPlot(data,y_train,"DBIndex ("+make+")")

  print("DBIndex, ARI,",metrics.adjusted_rand_score(y_train,target))
  print("DBIndex, MI,",normalized_mutual_info_score(y_train,target))

  #KMeans.
  KMEANS= KMeans(n_clusters=2, random_state=0).fit(data)
  y_train=KMEANS.labels_
  getPlot(data,y_train,"KMeans ("+make+")")
  print("KMeans, ARI,", metrics.adjusted_rand_score(y_train, target))
  print("KMeans, MI,", normalized_mutual_info_score(y_train, target))

  #DBSCAN
  model = DBSCAN(eps=3, min_samples=2).fit(data)
  y_pred = model.labels_.astype(np.int)
  getPlot(data,y_train,"DBSCAN ("+make+")")
  print("DBSCAN, ARI,", metrics.adjusted_rand_score(y_pred, target))
  print("DBSCAN, MI,", normalized_mutual_info_score(y_pred, target))

  #Aqui se reseulve utilizando el aglomerativo
  model = AgglomerativeClustering().fit(data)
  y_pred = model.labels_.astype(np.int)
  getPlot(data,y_train,"Agglomerative ("+make+")")
  print("Agglomerative, ARI,", metrics.adjusted_rand_score(y_pred, target))
  print("Agglomerative, MI,", normalized_mutual_info_score(y_pred, target))

  #FuzzyCMeans
  model = FuzzyCMeans(2, 1.2)
  model.fit(data)
  y_pred = model.predict()
  getPlot(data,y_pred,"FuzzyCMeans ("+make+")")
  print("FuzzyCMeans, ARI,", adjusted_rand_score(target, y_pred))
  print("FuzzyCMeans, MI,", adjusted_mutual_info_score(target, y_pred))
