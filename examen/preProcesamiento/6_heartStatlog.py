from sklearn.preprocessing import MinMaxScaler
import pandas as pd
import numpy as np

path = "/Users/drodriguez/Proyectos/Cinvestav/ciencia_datos/examen/"

headers = ["age", "sex", "chest_pain", "blood_pressure", "serum_cholesterol",
           "blood_sugar", "electrocardiographic", "max__heart_rate", "exexrcise_angina",
           "oldpeak", "slope", "major_vessels","thal"]

dataframe = pd.read_csv(path+'datasets/6_heart.csv', header=None, names=headers, na_values="?")

print(dataframe.dtypes)
print("Headers: ",len(headers))


### Reemplazo de Valores Faltantes ### 
# No missing values


print(dataframe.dtypes)
print("*"*40)
print(dataframe.isna().sum())

scaler = MinMaxScaler()
dataframe = scaler.fit_transform(dataframe)
dataframe = pd.DataFrame(dataframe)

dataframe.to_csv(path+'cleanData/heartStatlog.csv',index=False, header=False)
