from sklearn.preprocessing import MinMaxScaler
import pandas as pd
import numpy as np

path = "/Users/drodriguez/Proyectos/Cinvestav/ciencia_datos/examen/"

headers = ['C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'C9', 'C10', 'C11', 'C12',
           'C13', 'C14', 'C15', 'C16', 'C17', 'C18', 'C19', 'C20', 'C21', 'C22', 'C23',
           'C24', 'C25', 'C26', 'C27', 'C28', 'C29', 'C30', 'C31', 'C32', 'C33', 'C34',
           'C35', 'C36', 'C37', 'C38', 'C39', 'C40', 'C41', 'C42', 'C43', 'C44', 'C45',
           'C46', 'C47', 'C48', 'C49', 'C50', 'C51', 'C52', 'C53', 'C54', 'C55', 'C56',
           'C57', 'C58', 'C59', 'C60', 'C61', 'C62', 'C63', 'C64', 'C65', 'C66', 'C67',
           'C68', 'C69', "id", "class"
           ]



dataframe = pd.read_csv(path+'datasets/10_audiology.csv',
                        header=None, names=headers, na_values="?")

print(dataframe.head())
#print(dataframe.dtypes)

del dataframe['id']

### Conversion de categoricos a númericos continuos ###

dataframe.replace('f', value=0, inplace=True)
dataframe.replace('t', value=1, inplace=True)

cleanup_nums = {
    "C2": {"mild": 1, "moderate": 2, "severe":3, "normal":4, "profound":5 },
    "C4": {"normal": 1, "elevated": 2, "absent": 3},
    "C5": {"normal": 1, "absent": 2, "elevated": 3},
    "C6": {"mild": 1, "moderate": 2, "normal": 3, "unmeasured": 0},
    "C8": {"normal": 1, "degraded": 2},
    "C59": {"normal": 1, "elevated": 2, "absent": 3},
    "C60": {"normal": 1, "absent": 2, "elevated": 3},
    "C64": {"normal": 1, "good":2, "very_good":3, "very_poor":4, "poor":5, "unmeasured":0},
    "C66": {"a":1, "as":2, "b":3, "ad":4, "c":5},
    "class": {
        "acoustic_neuroma": 1, 
        "bells_palsy": 2,
        "cochlear_age": 3,
        "cochlear_age_and_noise": 4,
        "cochlear_age_plus_poss_menieres": 5,
        "cochlear_noise_and_heredity": 6,
        "cochlear_poss_noise": 7,
        "cochlear_unknown": 8,
        "conductive_discontinuity": 9,
        "conductive_fixation": 10,
        "mixed_cochlear_age_fixation": 11,
        "mixed_cochlear_age_otitis_media": 12,
        "mixed_cochlear_age_s_om": 13,
        "mixed_cochlear_unk_discontinuity": 14,
        "mixed_cochlear_unk_fixation": 15,
        "mixed_cochlear_unk_ser_om": 16,
        "mixed_poss_central_om": 17,
        "mixed_poss_noise_om": 18,
        "normal_ear": 19,
        "otitis_media": 20,
        "poss_central": 21,
        "possible_brainstem_disorder": 22,
        "possible_menieres": 23,
        "retrocochlear_unknown":24     
        }
    }

dataframe.replace(cleanup_nums, inplace=True)
#######################################################

#print(dataframe.head())

### Reemplazo de Valores Faltantes ### 
column = headers[3]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[4]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[5]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[7]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[58]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[59]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[63]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[65]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

######################################
### One hot enconding ###
#dataframe = pd.get_dummies(dataframe, columns=['class'])
#########################

### Mover la variable de clase al final ###
#cols = dataframe.columns.tolist()
#cols = cols[1:] + cols[0:1]
#dataframe = dataframe[cols]
###########################################

print(dataframe.head())
#print(dataframe.dtypes)
print(dataframe.isna().sum())

clase= dataframe["class"]
#### Escalado de los datos
scaler = MinMaxScaler()
dataframe = scaler.fit_transform(dataframe)
dataframe = pd.DataFrame(dataframe)
dataframe[69]=clase

print(dataframe.head())

dataframe.to_csv(path+'cleanData/10_audiology.csv', index=False, header=False)
