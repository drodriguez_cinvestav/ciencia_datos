from sklearn.preprocessing import MinMaxScaler
import pandas as pd
import numpy as np

headers = ["class", "age", "sex", "steroid", "antivirals",
           "fatigue", "malaise", "anorexia", "liver_big",
           "liver_firm", "spleen_palpable", "spiders", "ascites", 
           "varices","bilirubin", "alk_phosphate", "sgot", "albumin",
           "protime", "histology"]

dataframe = pd.read_csv('../datasets/4_hepatitis.csv',header=None, names=headers, na_values="?")

print(dataframe.dtypes)
print(len(headers))


### Reemplazo de Valores Faltantes ### 
column = headers[3]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[5]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[6]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[7]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[8]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[9]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[10]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[11]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[12]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[13]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[14]
print(column)
median = dataframe[column].median()
print("Median: ", median)
dataframe[column] = dataframe[column].fillna(median)

column = headers[15]
print(column)
median = dataframe[column].median()
print("Median: ", median)
dataframe[column] = dataframe[column].fillna(median)

column = headers[16]
print(column)
median = dataframe[column].median()
print("Median: ", median)
dataframe[column] = dataframe[column].fillna(median)

column = headers[17]
print(column)
median = dataframe[column].median()
print("Median: ", median)
dataframe[column] = dataframe[column].fillna(median)

column = headers[18]
print(column)
median = dataframe[column].median()
print("Median: ", median)
dataframe[column] = dataframe[column].fillna(median)


### Mover la variable de clase al final ###
cols = dataframe.columns.tolist()
cols = cols[1:] + cols[0:1]
dataframe = dataframe[cols]
###########################################

print(dataframe.dtypes)
print("*"*40)
print(dataframe.isna().sum())

scaler = MinMaxScaler()
dataframe = scaler.fit_transform(dataframe)
dataframe = pd.DataFrame(dataframe)

dataframe.to_csv('../cleanData/hepatitis.csv',index=False, header=False)
