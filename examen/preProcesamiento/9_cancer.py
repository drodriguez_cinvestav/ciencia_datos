from sklearn.preprocessing import MinMaxScaler
import pandas as pd
import numpy as np

path = "/Users/drodriguez/Proyectos/Cinvestav/ciencia_datos/examen/"

headers = ["class", "age", "menopause", "tumor_size", "inv_nodes",
           "node_caps", "deg_malig", "breast", "breast_quad","irradiat"]

dataframe = pd.read_csv(path+'datasets/9_breast-cancer.csv', header=None, names=headers, na_values="?")

print(dataframe.head())
#print(dataframe.dtypes)

### Conversion de categoricos a númericos continuos ###
cleanup_nums = {
    "class": {"no-recurrence-events": 0, "recurrence-events": 1},
    "age": {"10-19": 1, "20-29": 2, "30-39": 3, "40-49": 4, "50-59": 5, "60-69": 6, 
            "70-79": 7, "80-89": 8, "90-99": 9},
    "menopause": {"lt40": 1, "ge40": 2, "premeno": 3},
    "tumor_size": {"0-4": 1, "5-9": 2, "10-14": 3, "15-19": 4, "20-24": 5, "25-29": 6, "30-34": 7, 
                   "35-39": 8, "40-44": 9, "45-49": 10, "50-54": 11, "55-59": 12},
    "inv_nodes": {"0-2": 1, "3-5": 2, "6-8": 3, "9-11": 4, "12-14": 5, "15-17": 6, "18-20": 7, 
                  "21-23": 8, "24-26": 9, "27-29": 10, "30-32":11, "33-35":12, "36-39":13},
    "node_caps": {"no": 0, "yes": 1},
    "breast": {"left": 0, "right": 1},
    "breast_quad": {"left_up": 0, "left_low": 1, "right_up": 2, "right_low": 3, "central":4},
    "irradiat": {"yes":0,"no":1}
    }

dataframe.replace(cleanup_nums, inplace=True)
#######################################################

print(dataframe.head())

### Reemplazo de Valores Faltantes ### 
column = headers[5]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[8]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

######################################
### One hot enconding ###
#dataframe = pd.get_dummies(dataframe, columns=['breast_quad','menopause'])
#########################

### Mover la variable de clase al final ###
cols = dataframe.columns.tolist()
cols = cols[1:] + cols[0:1]
dataframe = dataframe[cols]
###########################################

print(dataframe.head())
#print(dataframe.dtypes)
#print(dataframe.isna().sum())

#### Escalado de los datos
scaler = MinMaxScaler()
dataframe = scaler.fit_transform(dataframe)
dataframe = pd.DataFrame(dataframe)

print(dataframe.head())

dataframe.to_csv(path+'cleanData/9_breast-cancer.csv', index=False, header=False)
