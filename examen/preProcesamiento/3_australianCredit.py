from sklearn.preprocessing import MinMaxScaler
import pandas as pd
import numpy as np

headers = ["a1", "a2", "a3", "a4", "a5",
           "a6", "a7", "a8", "a9", "a10",
           "a11", "a12", "a13", "a14", "class",
           ]

dataframe = pd.read_csv('../datasets/3_australian.csv',header=None, names=headers, na_values="?")

print(dataframe.dtypes)
print(dataframe.describe())

#### Escalado de los datos
scaler = MinMaxScaler()
dataframe = scaler.fit_transform(dataframe)
dataframe = pd.DataFrame(dataframe)

dataframe.to_csv('../cleanData/australianCredit.csv',index=False, header=False)
