from sklearn.preprocessing import MinMaxScaler
import pandas as pd
import numpy as np

path = "/Users/drodriguez/Proyectos/Cinvestav/ciencia_datos/examen/"

headers = ["surgery", "age", "hospital_number", "rectal_temperature", "pulse",
           "respiratory_rate", "temp_extremities", "peripheral_pulse", "mucous_membranes", 
           "capillary_refill", "pain", "peristalsis", "abdominal_distension",
           "nasogastric_tube", "nasogastric_reflux", "nasogastric_reflux_hp","feces",
           "abdomen", "packed_cell_volume", "total_protein", "abdominocentesis_appearance",
           "abdomcentesis_total_protein", "outcome", "surgical_lesion","type_lesion_1",
           "type_lesion_2", "type_lesion_3","cp_data"]

dataframe = pd.read_csv('datasets/8_horse.csv', header=None, names=headers, na_values="?")

print("Headers: ",len(headers))
print(dataframe.head())
print(dataframe.dtypes)
print(dataframe.describe())

### Reemplazo de Valores Faltantes ### 
column = headers[0]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[3]
print(column)
median = dataframe[column].median()
print("Median: ", median)
dataframe[column] = dataframe[column].fillna(median)

column = headers[4]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[5]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[6]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[7]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[8]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[9]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[10]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[11]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[12]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[13]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[14]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[15]
print(column)
median = dataframe[column].median()
print("Median: ", median)
dataframe[column] = dataframe[column].fillna(median)

column = headers[16]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[17]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[18]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[19]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[20]
print(column)
median = dataframe[column].median()
print("Median: ", median)
dataframe[column] = dataframe[column].fillna(median)

column = headers[21]
print(column)
median = dataframe[column].median()
print("Median: ", median)
dataframe[column] = dataframe[column].fillna(median)

column = headers[22]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

######################################
### One hot enconding ###
#dataframe = pd.get_dummies(dataframe, columns=['make','fuel_type','aspiration','body_style','drive_wheels','engine_location','engine_type','fuel_system'])
#########################

### Mover la variable de clase al final ###
#cols = dataframe.columns.tolist()
#cols = cols[1:] + cols[0:1]
#dataframe = dataframe[cols]
###########################################

print(dataframe.dtypes)
print(dataframe.isna().sum())

#### Escalado de los datos
scaler = MinMaxScaler()
dataframe = scaler.fit_transform(dataframe)
dataframe = pd.DataFrame(dataframe)

print(dataframe.head())

dataframe.to_csv(path+'cleanData/8_hoser.csv',index=False, header=False)
