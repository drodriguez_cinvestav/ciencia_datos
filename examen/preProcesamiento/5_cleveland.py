from sklearn.preprocessing import MinMaxScaler
import pandas as pd
import numpy as np

path = "/Users/drodriguez/Proyectos/Cinvestav/ciencia_datos/examen/"

headers = ["age", "sex", "cp", "trestbps", "chol",
           "fbs", "restecg", "thalach", "exang",
           "oldpeak", "slope", "ca", "thal","class"]

dataframe = pd.read_csv(path+'datasets/5_cleveland.csv', header=None, names=headers, na_values="?")

print(dataframe.dtypes)
print(len(headers))


### Reemplazo de Valores Faltantes ### 
column = headers[11]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[12]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)


print(dataframe.dtypes)
print("*"*40)
print(dataframe.isna().sum())

scaler = MinMaxScaler()
dataframe = scaler.fit_transform(dataframe)
dataframe = pd.DataFrame(dataframe)

dataframe.to_csv(path+'cleanData/cleveland.csv',index=False, header=False)
