from sklearn.preprocessing import MinMaxScaler
import pandas as pd
import numpy as np

path = "/Users/drodriguez/Proyectos/Cinvestav/ciencia_datos/examen/"

headers = ["A1", "A2", "A3", "A4", "A5",
           "A6", "A7", "A8", "A9", 
           "A10", "A11", "A12", "A13",
           "A14", "A15", "class"]

dataframe = pd.read_csv('datasets/7_credit.csv', header=None, names=headers, na_values="?")

print(dataframe.head())
print(dataframe.dtypes)

### Conversion de categoricos a númericos continuos ###
cleanup_nums = {
    "A1": {"a": 0, "b": 1},
    "A4": {"u": 0, "y": 1, "l": 2, "t": 3},
    "A5": {"g": 0, "p": 1, "gg":2},
    "A6": {"c": 0, "d": 1, "cc":2, "i":3, "j":4, "k":5, "m":6, "r":7, "q":8, "w":9, "x":10, "e":11, "aa":12, "ff":13},
    "A7": {"v": 0, "h": 1, "bb":2, "j":3, "n":4, "z":5, "dd":6, "ff":7, "o":8},
    "A9": {"t": 0, "f": 1},
    "A10":{"t": 0, "f": 1},
    "A12":{"t": 0, "f": 1},
    "A13":{"g": 0, "p": 1, "s": 2},
    "class": {"+": 0, "-": 1}
    }

dataframe.replace(cleanup_nums, inplace=True)
#######################################################

### Reemplazo de Valores Faltantes ### 
column = headers[0]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[1]
print(column)
median = dataframe[column].median()
print("Median: ", median)
dataframe[column] = dataframe[column].fillna(median)

column = headers[3]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[4]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[5]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[6]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

column = headers[13]
print(column)
mode = dataframe[column].mode()[0]
print("Moda: ", mode)
dataframe[column] = dataframe[column].fillna(mode)

######################################
### One hot enconding ###
#dataframe = pd.get_dummies(dataframe, columns=['make','fuel_type','aspiration','body_style','drive_wheels','engine_location','engine_type','fuel_system'])
#########################

### Mover la variable de clase al final ###
#cols = dataframe.columns.tolist()
#cols = cols[1:] + cols[0:1]
#dataframe = dataframe[cols]
###########################################

print(dataframe.dtypes)
print(dataframe.isna().sum())

#### Escalado de los datos
scaler = MinMaxScaler()
dataframe = scaler.fit_transform(dataframe)
dataframe = pd.DataFrame(dataframe)

print(dataframe.head())

dataframe.to_csv(path+'cleanData/7_credit.csv',index=False, header=False)
