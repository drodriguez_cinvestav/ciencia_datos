from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler
from seaborn import distplot
import pandas as pd
import numpy as np

headers = ["class", "normalized_losses", "make", "fuel_type", "aspiration",
           "num_doors", "body_style", "drive_wheels", "engine_location",
           "wheel_base", "length", "width", "height", "curb_weight",
           "engine_type", "num_cylinders", "engine_size", "fuel_system",
           "bore", "stroke", "compression_ratio", "horsepower", "peak_rpm",
           "city_mpg", "highway_mpg", "price"]

dataframe = pd.read_csv('datasets/1_autos.csv',header=None, names=headers, na_values="?")

#dataframe.dtypes

### Conversion de categoricos a númericos continuos ###
cleanup_nums = {"num_doors":     {"four": 4, "two": 2},
                "num_cylinders": {"four": 4, "six": 6, "five": 5, "eight": 8,
                                  "two": 2, "twelve": 12, "three":3 }}

dataframe.replace(cleanup_nums,inplace=True)
#######################################################

#distplot(dataframe["num_doors"])
#plt.show()

### Reemplazo de Valores Faltantes ###
median = dataframe["normalized_losses"].median()
dataframe["normalized_losses"] = dataframe["normalized_losses"].fillna(median)

mode = dataframe["num_doors"].mode()[0]
dataframe["num_doors"] = dataframe["num_doors"].fillna(mode)

median = dataframe["bore"].median()
dataframe["bore"] = dataframe["bore"].fillna(median)

median = dataframe["stroke"].median()
dataframe["stroke"] = dataframe["stroke"].fillna(median)

median = dataframe["horsepower"].median()
dataframe["horsepower"] = dataframe["horsepower"].fillna(median)

median = dataframe["peak_rpm"].median()
dataframe["peak_rpm"] = dataframe["peak_rpm"].fillna(median)

median = dataframe["price"].median()
dataframe["price"] = dataframe["price"].fillna(median)
######################################

### One hot enconding ###
dataframe = pd.get_dummies(dataframe, columns=[ 'make','fuel_type','aspiration',
                                                'body_style','drive_wheels','engine_location',
                                                'engine_type','fuel_system'])
#########################

### Mover la variable de clase al final ###
cols = dataframe.columns.tolist()
cols = cols[1:] + cols[0:1]
dataframe = dataframe[cols]
###########################################
#print(dataframe.dtypes)
#print(dataframe.isna().sum())

#print(dataframe.head())

## Escalado de los datos
scaler = MinMaxScaler()
#scaler = StandardScaler()
dataframe = scaler.fit_transform(dataframe)
dataframe = pd.DataFrame(dataframe)

print(dataframe.head())


dataframe.to_csv('cleanData/1_autos.csv',index=False, header=False)
