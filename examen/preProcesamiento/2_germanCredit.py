from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np

headers = ["checking_account", "duration", "credit_history", "purpose", "credit_amount",
           "savings", "employment", "disposable_income", "personal_status_sex", "debtors",
           "residence_since", "property", "age", "installment_plans", "housing", "credits",
           "job", "people_maintenance", "telephone", "foreign_worker", "class"]
#print(len(headers))

dataframe = pd.read_csv('datasets/2_german.csv',header=None, names=headers, na_values="?")
#print(dataframe.dtypes)
print(dataframe.head())


### Conversion de categoricos a númericos continuos ###

cleanup_nums = {
               
                "telephone": {"A191": 0, "A192": 1},
                "foreign_worker": {"A201": 0, "A202": 1},
                }

dataframe.replace(cleanup_nums, inplace=True)

#######################################################

### One hot enconding ###
dataframe = pd.get_dummies(
    dataframe, columns=['checking_account', 'credit_history', 'purpose', 'savings', 
                        'employment', 'personal_status_sex', 'debtors', 'property', 
                        'installment_plans', 'housing', 'job'])
#########################


### Reemplazo de Valores Faltantes ### 
# Sin datos faltantes
###########################################

#print(dataframe.dtypes)
print(dataframe.head())
print(dataframe.isna().sum())

#### Escalado de los datos
scaler = MinMaxScaler()
dataframe = scaler.fit_transform(dataframe)
dataframe = pd.DataFrame(dataframe)

dataframe.to_csv('cleanData/2_german.csv', index=False, header=False)

