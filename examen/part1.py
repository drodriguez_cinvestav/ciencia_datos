from sklearn import cluster, datasets
from sklearn.metrics.cluster import adjusted_rand_score, adjusted_mutual_info_score
from sklearn import metrics
from os import listdir
import numpy as np
import pandas as pd

def k_means(k,X,y):
    k_means=cluster.KMeans(n_clusters=k, random_state=0)
    k_means.fit(X)
    y_pred=k_means.predict(X)

    #Externals Index
    ARI=adjusted_rand_score(y, y_pred)
    print("Adjusted Rand Index", ARI)
    #Mutual Information
    MI=adjusted_mutual_info_score(y, y_pred)
    print("Mutual Information",MI)
    return [ARI,MI]

def dbscan(X,y):
    model=cluster.DBSCAN(eps=0.5, min_samples=5)
    model.fit_predict(X)
    y_pred=model.labels_.astype(np.int)
    #print(y_pred)
    #Externals Index
    ARI=adjusted_rand_score(y, y_pred)
    print("Adjusted Rand Index", ARI)
    #Mutual Information
    MI=adjusted_mutual_info_score(y, y_pred)
    print("Mutual Information",MI)
    return [ARI,MI]

def agglomerative(k,X,y):
    model = cluster.AgglomerativeClustering(n_clusters=k)
    model = model.fit(X)
    y_pred=model.labels_.astype(np.int)
    #print(y_pred)
    #Externals Index
    ARI=adjusted_rand_score(y, y_pred)
    print("Adjusted Rand Index", ARI)
    #Mutual Information
    MI=adjusted_mutual_info_score(y, y_pred)
    print("Mutual Information",MI)
    return [ARI,MI]

if __name__ == "__main__":
            
    path = '/Users/drodriguez/Proyectos/Cinvestav_code/ciencia_datos/examen/cleanData/'
    ks = [3,23, 7, 2, 2, 2, 5, 2, 2, 2, 2]
    archivos = listdir(path)
    if '.DS_Store' in archivos:
        archivos.remove('.DS_Store')
    archivos = sorted(archivos)
    print(archivos)
    with open('resultado_part1.csv', 'w') as file:
        file.write("File,K,KMEANS_ARI,KMEANS_MI,DBSCAN_ARI,DBSCAN_MI,AGLOME_ARI,AGLOME_MI\n")
        for id,archivo in enumerate(archivos):
            print("archivo: {}, k: {}".format(archivo,ks[id]))
            data = pd.read_csv(path+archivo, header=None)
            k = ks[id]
            #print(data.head())
            #print(len(data.columns))
            X = data.iloc[:, 0:len(data.columns)-1]
            y = data.iloc[:, -1]
            #print(y)
            #print(X)
            print("Data Set: ",archivo)
            print("k", k)
            print("K_MEANS")
            KM_ARI, KM_MI = k_means(k, X, y)

            print("\nDBSCAN")
            DB_ARI, DB_MI =  dbscan(X, y)

            print("\nAgglomerativo")
            AG_ARI, AG_MI =  agglomerative(k, X, y)
            
        
            file.write("{},{},{},{},{},{},{},{}\n".format(archivo, k, KM_ARI, KM_MI, DB_ARI, DB_MI, AG_ARI, AG_MI))
            
            print("#"*50)