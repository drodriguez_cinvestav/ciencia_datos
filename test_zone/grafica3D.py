if __name__ == "__main__":
    #Data
    data = pd.read_csv("data.csv")
    #print(type(data))
    #print(data.shape)
    #print(data.head())

    p = Neurona()
    p.trainig(data)

    # Creamos la figura
    fig = plt.figure()
    # Creamos el plano 3D
    ax1 = fig.add_subplot(111, projection='3d')

    # Definimos los datos de prueba
    #x = [1,2,3,4,5,6,7,8,9,10]
    #y = [5,6,7,8,2,5,6,3,7,2]
    #z = [1,2,6,3,2,7,3,3,7,2]
    x=data.iloc[:, 0] #df.loc[:, 'X1']
    y=data.iloc[:, 1]
    z=data.iloc[:, 2]
    #print("x:{}, y:{}, z:{}".format(x,y,z))

    # Datos adicionales
    #x2 = [-1,-2,-3,-4,-5,-6,-7,-8,-9,-10]
    #y2 = [-5,-6,-7,-8,-2,-5,-6,-3,-7,-2]
    #z2 = [1,2,6,3,2,7,3,3,7,2]
    
    # Agregamos los puntos en el plano 3D
    ax1.scatter(x, y, z, c='g', marker='o')
    #ax1.scatter(x2, y2, z2, c ='r', marker='o')

    # Mostramos el gráfico
    #plt.show()