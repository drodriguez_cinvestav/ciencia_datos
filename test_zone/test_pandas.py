import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split

if __name__ == "__main__":
    #read data from .csv file
    path='/Users/drodriguez/proyectos/cinvestav/ciencia_datos/data/'
    data = pd.read_csv(path+"LinealmenteSeparable.csv",delimiter = ',')
    data.columns = ["X1", "X2", "C"]
    classes = data["C"]
    data = data.drop(columns="C")

    print(data)
    print(data.shape)

    filas=data.shape[0]
    columnas=data.shape[1]

    ##Recorrre por columnas
    for col in data:
        print(col)
        print(data[col].values)
        for i in range(filas):
            print(data[col].values[i])
    
    print("#"*30)
    for indice_fila, fila in data.iterrows():
        print(indice_fila)
        print(fila.values)

    #splitting test and train data for iris
    x_train, x_test, y_train, y_test = train_test_split(data, classes)
    
    print(x_train.head())
    print(y_train.head())

    print(np.asarray(y_train))
    