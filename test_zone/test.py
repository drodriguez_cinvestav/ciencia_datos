from neurona import *
import pandas as pd 
import numpy as np

if __name__ == "__main__":
    p = Neurona()
    data = pd.read_csv("data.csv") 
    print(data.shape)
    print(data.head())
    fila = data.iloc[0] #Obtencion del vector n
   
    df = pd.DataFrame({'uno': [1, 2, 3], 'dos': [4, 5, 6], 'tres': [7, 8, 9]}, index=['x', 'y', 'z'])

    # Iteración por columnas del DataFrame:
    for col in df:
        print(df[col].mean())
        
    print(30*"#")

    # Iteración por filas del DataFrame:
    for indice_fila, fila in df.iterrows():
        #print(indice_fila)
        print(fila)
	
