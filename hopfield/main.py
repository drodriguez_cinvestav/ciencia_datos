import os
from os import scandir
import numpy as np
import matplotlib.pyplot as plt
import skimage.io as io
from skimage.color import rgb2gray
from numpy import zeros, outer, diag_indices
import random

def imgTobin(imagen):
    #print(type(imagen))
    filas,columnas=img_gray.shape
    #print(filas)
    #print(columnas)
    for f in range(filas):
        for c in range(columnas):
            pixel = imagen[f][c]
            if pixel > 0.5:
                imagen[f][c] = 1
            else:
                imagen[f][c] = -1
    return imagen

def aplanar(imagen):
    filas,columnas=img_gray.shape
    vector=np.array([])
    #print(filas)
    #print(columnas)
    for f in range(filas):
        for c in range(columnas):
            pixel = imagen[f][c]
            vector=np.append(vector,pixel)
    return vector

def deformar(imagen,pixeles):
    filas,columnas=img_gray.shape

    for p in range(pixeles):
        fila_ran=random.randrange(0,filas)
        col_ran=random.randrange(0,columnas)
        pixel=imagen[fila_ran,col_ran]
        if pixel<0:
            new_pixel=1
        else:
            new_pixel=-1
        imagen[fila_ran,col_ran]=new_pixel
    return imagen

def fit(patterns):
    r,c = len(patterns),1024
    W = zeros((c,c))
    for p in patterns:
        W = W + outer(p,p) #recall that the outner product give us a matrix u*t(v). Different to inner product
    W[diag_indices(c)] = 0
    return W/r
    
def predict(W, patterns, steps):
    from numpy import vectorize, dot
    sgn = vectorize(lambda x: -1 if x<0 else +1)#Lambda expression
    for _ in range(steps):
        patterns = sgn(dot(patterns,W))
    print("Predict: ",patterns)
    return patterns

def getIconos(ruta):
    files = [arch.name for arch in scandir(ruta) if arch.is_file()]
    if '.DS_Store' in files:
        files.remove('.DS_Store')
    print("Archivos: ",files)
    return files

if __name__ == "__main__":
    ruta='data/'
    iconos=getIconos(ruta)
    imagenes_bin=[]
    patrones=[]
    for f in iconos:
        img = io.imread(ruta+f)
        img_gray = rgb2gray(img)
        #print(img_gray.shape)
        img_bin=imgTobin(img_gray)
        #io.imshow(img_bin)
        #io.show()
        imagenes_bin.append(img_bin)
        icono_aplastado=aplanar(img_bin)
        patrones.append(icono_aplastado)

    #io.imshow(imagenes_bin[0])
    #io.show()

    w = fit(patrones)
    print("W:")
    print(w)

    imagen_deformada=deformar(imagenes_bin[4],800)
    imagen_deformada_aplanada=aplanar(imagen_deformada)
    io.imshow(imagen_deformada)
    io.show()

    patrones_predecidos=predict(w, imagen_deformada_aplanada, 1)
    print("Tam patrones: ",len(patrones_predecidos))
    imagen_reconstruida=np.reshape(patrones_predecidos, (32, 32), order='C')
    #imagen_reconstruida=np.reshape(imagen_deformada_aplanada, (32, 32), order='C')
    io.imshow(imagen_reconstruida)
    io.show()
