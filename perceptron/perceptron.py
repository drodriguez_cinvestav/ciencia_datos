import pandas as pd
import numpy as np
import math

class Perceptron:
    w=[]

    def __init__(self):
        super().__init__()
       

    def fit(self, X, y, n, epocas):
        #initializing weight vector
        self.w=[0.0 for i in range(len(X.columns)+1)]
        print("W_init:{}".format(self.w))
        #no.of iterations to train the neural network
        for e in range(epocas):
            #print(str(epoch+1),"epoch has started...")
            for index in range(len(X)):
                x=X.iloc[index]
                predicted=self.activacion(x)
                #check for misclassification
                if(y.iloc[index]==predicted):
                    pass
                else:
                    #calculate the error value
                    error=y.iloc[index]-predicted
                    #updation of threshold
                    self.w[0]=self.w[0]+n*error
                    #updation of associated self.weights acccording to Delta rule
                    for j in range(len(x)):
                        self.w[j+1]=self.w[j+1]+n*error*x[j]
    
    def activacion(self, data):
        #initializing with threshold value
        activation_val = self.w[0]
        activation_val += np.dot(self.w[1:], data)
        return 1 if activation_val >= 0 else 0
    
    def predict(self, x_test):
        predicted = []
        for i in range(len(x_test)):
            #prediction for test set using obtained weights
            predicted.append(self.activacion(x_test.iloc[i]))
        return predicted

    def accuracy(self, predicted, original):
        correct = 0
        lent = len(predicted)
        for i in range(lent):
            if(predicted[i] == original.iloc[i]):
                correct += 1
        return (correct/lent)*100

    def getW(self):
        return self.w
