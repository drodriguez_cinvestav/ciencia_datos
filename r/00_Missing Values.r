
###########################################################################
#Description: Allows us to show the effects of the missing values in a dataset
#package
#Author: Edwin Aldana, ealdana@tamps.cinvestav.mx
###########################################################################


#####################################################################################
# Dependencies
#####################################################################################

#####################################################################################


#####################################################################################
# Data and input parameters
#####################################################################################
#Load data from local repository 
data <- read.csv("data/communities.data.txt",sep="\t", header=TRUE)

#####################################################################################


#####################################################################################
# Main Process
#####################################################################################
#STEP 1: Determine the columns that have missing values
i=1
numCols=length(data[1,])
while(i<=numCols)
{ numberOfColumn=i
  missingValues=length(which(is.na(data[,numberOfColumn])))
  print(paste("column:",numberOfColumn,"count:",missingValues))
  i=i+1
}

#Mean imputation:replace each missing value with the mean of the observed values for that variable.
#Example for the column 2
columnNumber=3
windows()
par(mfrow=c(2,2))
missingValues=length(which(is.na(data[,columnNumber])))

min=min(data[which(!is.na(data[,columnNumber])),columnNumber])
mu=mean(data[which(!is.na(data[,columnNumber])),columnNumber])
std=sd(data[which(!is.na(data[,columnNumber])),columnNumber])
hist(data[which(!is.na(data[,columnNumber])),columnNumber],main="Histogram deleting missing values")
plot(density(data[which(!is.na(data[,columnNumber])),columnNumber]),"Density deleting missing values")

valoresNuevos=runif(missingValues,(mu-2*std),(mu+2*std))
#valoresNuevos[which(valoresNuevos<min)]=min
data[which(is.na(data[,columnNumber])),columnNumber]<-valoresNuevos
#data[which(is.na(data[,columnNumber])),columnNumber]=mu
hist(data[,columnNumber],main="Histogram with imputation strategy")
plot(density(data[,columnNumber]),"Density with mean imputation")


#####################################################################################
