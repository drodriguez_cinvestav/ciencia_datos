#---------------------------------------------------------
# 1. VARIABLES, VECTORES Y MATRICES 
#---------------------------------------------------------

# 1.1 INTRODUCCI�N < 000 >
# 1.2 DECLARAR, BORRAR Y GUARDAR VARIABLES < 006 >
# 1.3 VECTORES < 009 >
# 1.4 DATOS ALFANUM�RICOS (TEXTO)  < 037 >
# 1.5 MATRICES  < 044 >

# Solo tienes que copiar-pegar-ejecutar cada bloque de c�digo en la l�nea de
# comandos de R y analizar el resultado.

# < 000 >  

# 1.1 INTRODUCCI�N

# 1976 : Lenguaje S, John Chambers, Bell Laboratories (EUA)
# 1990s: Lenguaje R, Ross Ihaka & Robert Gentleman (Australia)
# 1997 : R Development Core Team, www.r-project.org

# manuales PDF

# Computational Statistics

# Use R! Springer series

q()

help(solve)
help("[[")
?solve

help.start()

example(solve)

example(persp)

help.search("multivariate normal")


# < 001 >

# case sensitive

# Men� > Editar > Preferencias de la interface gr�fica > ...


# < 002 >

# Calculadora, operaciones usuales:

#   + - * / ^  %/%  %%

7%/%3

7%%3

# Funciones usuales:

# log exp sqrt abs sin cos tan asin acos atan

# factorial choose(n,x)  gamma  lgamma

# floor ceiling trunc round


floor(3.8)

ceiling(3.1)

trunc(3.8)

pi

round(pi,0)

round(pi,2)


# < 003 >

# NaN  0/0   Inf  -Inf  1/0

2/0

-3/0

0/0

2/Inf

2*Inf

3-Inf


# < 004 >

# N�meros complejos:

3+2i

0+1i

(0+1i)^2

4+0i

Re(3+2i)

Im(3+2i)



# < 005 >

# Operaciones l�gicas

# T o TRUE, F o FALSE

# <  <=  >  >=  ==  != 

# ! & | xor

# is.finite  is.infinite  is.nan

# funciones indicadoras

4 < 4

4 <= 4

4 == 3

4 != 3

T

F

!T

!F

T&T

T&F

F&F

T*T

T*F

F*F

T|T

T|F

F|F

xor(T,T)

xor(T,F)

xor(F,F)

is.finite(10)

is.infinite(10)

is.nan(10)

is.finite(Inf)

is.infinite(Inf)

is.nan(Inf)

is.finite(NaN)

is.infinite(NaN)

is.nan(NaN)


# < 006 >

# 1.2 DECLARAR, BORRAR Y GUARDAR VARIABLES

a <- 3 # tambi�n es v�lido assign("a", 3)

A <- -4

b <- 2*A

a

a+A

b

# < 007 >

ls()

exists("b")

rm("b")

exists("b")

ls()

rm(list=ls())

ls()

a <- 3
A <- -4
b <- 2*A
ls()

# < 008 >

# Archivo > Guardar �rea de trabajo... Sesi�n1.RData, o bien

# save.image("C:\\directorio\\Sesi�n1.RData")

# Archivo > Guardar en Archivo ... Sesi�n1.txt

q()   #  �Guardar imagen de �rea de trabajo? NO (ya lo hicimos)

# Doble click en Sesi�n1.RData: inicia R y carga archivo, o bien

# Iniciar R y luego: Archivo > Cargar �rea de trabajo... Sesi�n1.RData, o bien

# load("C:\\directorio\\Sesi�n1.RData")

ls()


# < 009 >

# 1.3 VECTORES

# vector

w <- vector(mode="numeric",length=80)

w

# O bien 

w <- numeric(80) # tambi�n es v�lido: assign("w",numeric(80))

w

w[3] <- 5

w[9] <- Inf

w

w[3]

w[9]


# < 010 >

w <- vector(mode="logical",length=7)

w

# O bien:

w <- logical(7)

w

w[2] <- T

w

w[3] <- 5

w


# < 011 >

w <- vector(mode="complex",length=10)

w

# O bien:

w <- complex(10)

w

w[4] <- 3

w

w[5] <- 2-4i

w


# < 012 >

# scan

y <- scan()

y


# < 013 >

# c  (concatenate) 

y <- c(0.3,-9,1)

y

y <- c(y,4)

y

y <- c(5,y)

y


# < 014 >

z <- vector(mode="numeric",length=0)

z

# O bien:

z <- numeric(0)

z

z <- c(z,3)

z

z <- c(z,3)

z

c(z,z)


# < 015 >

# Funciones usuales:

# log exp sqrt abs sin cos tan asin acos atan

# gamma  lgamma floor ceiling trunc round


x <- c(-3.2,0,2.9,Inf,NaN)

x

exp(x)

log(x)

floor(x)

ceiling(x)


# < 016 >

# Funciones vectoriales:

# min max range length sum prod diff

# cumsum cumprod cummax cummin pmax pmin

# sort rank order rev

# mean median var sd

x <- c(5,-1,2,-4,-99,66)

min(x)

max(x)

range(x)

length(x)

sum(x)

prod(x)

diff(x)

cumsum(x)

cumprod(x)

cummax(x)

cummin(x)


# < 017 >

y <- c(-3,2,4,10,-5,30)

x

y

pmax(x,y)

pmin(x,y)


# < 018 >

x

sort(x)

rank(x)

order(x)

rev(x)

rev(sort(x))


# < 019 >

x

median(x)

sum(x)/length(x)

mean(x)

var(x)

sqrt(var(x))

sd(x)


# < 020 >

# Sucesiones:

#  m:n  seq  rep 

2:11

14:5

seq(from = -1 , to = 2 , by = 0.2)

seq(from = 2 , to = 3 , length = 11)

rep(3, 10 )

rep(c(1, 3, 5), times = 4)

rep(c(1, 3, 5), each = 4)


# < 021 >

# Extrayendo subvectores:

# x[n] x[-n] x[m:n] x[-(m:n)] x[y]

x <- seq( from = 2 , to = 3 , length = 11)

x

x[2]

x[-2]

x[3:5]

x[-(3:5)]


# < 022 >

y <- c(1,3,11)

x[y]

x[-y]


# < 023 >

x <- c(5,-1,2,-4,-99,66)

x

sort(x)

rank(x)

order(x)

x[order(x)]


# < 024 >

# Operaciones usuales con vectores:

#   + - * / ^  %/%  %%

x <- 1:10

x

x+1

x*10

1/x

x^2

x%/%3

x%%3


# < 025 >

y <- 100:109

x

y

x+y

x-y

x*y

y/x

y^x


# < 026 >

y <- c(2,4,7)

x+y

x

cumsum(x)/(1:length(x))

mean(x)


# < 027 >

# Operaciones l�gicas con vectores: 

# is.na match which which.min which.max all any unique duplicated subset
# complete.cases

x <- 1:10

x

x > 3

1*(x>3)

x[(x>3)&(x<8)]


# < 028 >

z <- c(-1,NA,4,3,1,NA,0,Inf,NaN)

z

is.na(z)

z[is.na(z)] <- 100

z

x <- c(6,1:3,NA,12)

x[x>5]

subset(x,x>5)

x

(y <- c(NA,NA,3,4,5,6))

(ind <- complete.cases(x,y))

# Lo anterior forma para evitar escribir:

(!(is.na(x)))&(!(is.na(y)))

x;y

x[ind];y[ind]


# < 029 >

x <- c(4,rep(3,2),rep(5,3),10,5)

x

match(5,x)

match(10,x)

match(1,x)


# < 030 >

which(x==5)

which(x==1)

which(x>4)

x[which(x>4)]

x[x>4]


# < 031 >

which.min(x)

x[which.min(x)]

min(x)

which.max(x)

x[which.max(x)]

max(x)


# < 032 >

all(x>2)

all(x>3)

any(x>3)

any(x>10)


# < 033 >

x

unique(x)

duplicated(x)


# < 034 >

# Operaciones de conjuntos con vectores

# union intersect setdiff setequal is.element %in%

x <- 1:5

y <- 3:9

x

y

union(x,y)

intersect(x,y)

setdiff(x,y)

setdiff(y,x)

setequal(x,y)


# < 035 >

x

rev(x)

setequal(x,rev(x))

x==rev(x)

all(x==rev(x))

y <- c(3,3,6,7)

z <- c(3,7,6)

setequal(y,z)


# < 036 >

is.element(3,y)

is.element(9,y)

is.element(c(3,9),y)

# o bien:

3%in%y

c(3,9)%in%y


# < 037 >

# 1.4 DATOS ALFANUM�RICOS (TEXTO)

# nchar paste substr strsplit table(strsplit(...)) 

# tolower toupper noquote


"FES Acatl�n"  # El curso de R

a <- "FES Acatl�n"  # El curso de R

b <- "UNAM"

a

b

nchar(b)

length(b)


# < 038 >

d <- c(a,b)

d

nchar(d)

length(d)

e <- paste(a,b,2011)

e

nchar(e)

length(e)


# < 039  >

rep("No quiero",10)

w <- vector(mode="character",length=8)

w

# O bien:

w <- character(8)

w

w[c(3,5)] <- c("Posici�n 3","Posici�n 5") 

w


# < 040 >

y <- c("a","b","c","d")

z <- c("c","d","e","f")

intersect(y,z)

which(z=="e")


# < 041 >

colores <- c("rojo","amarillo","azul","verde")

colores

paste("perro",colores)

substr(colores,1,3)

substr(colores,1,6)

substr(colores,2,5)

substr(colores,6,10)


# < 042 >

frase <- "No por mucho madrugar amanece m�s temprano"

strsplit(frase,split=character(0))

strsplit(frase,split="a")

strsplit(frase,split="ad")

strsplit(frase,split="aw")

table(strsplit(frase,split=character(0)))


# < 043 >

frase <- "Al que madruga Dios lo arruga"

toupper(frase)

tolower(frase)

noquote(frase)


# < 044 >

# 1.5 MATRICES

# matrix cbind rbind colnames rownames edit array

matriz <- matrix(nrow=3,ncol=4)

matriz

matriz[2,3] <- 4.9

matriz


# < 045 >

matriz[,4] <- c(2,5,7)

matriz

matriz[3,] <- rep(1,4)

matriz


# < 046 >

matriz <- matrix(1:12,nrow=3,ncol=4)

matriz

# O bien:

m <- 1:10

m

dim(m) <- c(2,5)

m

matriz <- matrix(1:12,nrow=3,ncol=4,byrow=T)

matriz


# < 047 >

matriz[3, 2] # Entrega vector de longitud 1

matriz[3, 2, drop=F] # Entrega matriz de 1x1

matriz[ , 2] # Entrega vector con elementos de la columna 2

matriz[ , 2, drop=F] # Entrega submatriz con elementos de la columna 2

matriz[3, ] # Entrega vector con elementos de la fila 3

matriz[3, , drop=F] # Entrega submatriz con elementos de la fila 3

matriz[2:3, 3:4]

matriz[c(1,3), c(1,2,4)]


# < 048 >

matriz

matriz2 <- rbind(matriz,matriz)

matriz2

matriz3 <- cbind(matriz,matriz)

matriz3


# < 049 >

x <- 1:10

y <- 11:20

matriz4 <- rbind(x,y)

matriz4

matriz5 <- cbind(x,y)

matriz5


# < 050 >

colnames(matriz5) <- c("Columna 1","Columna 2")

matriz5

rownames(matriz5) <- paste("Fila",1:10)

matriz5

# O tambi�n:

m <- matrix(1:4, nrow = 2, ncol = 2)

dimnames(m) <- list(c("a","b"),c("c","d"))

m


# < 051 >

matriz5 <- edit(matriz5)

matriz5


# < 052 >

cubo <- array(1:24,dim=c(4,3,2))

cubo

cubo[3,1,2]


# < 053 >

matriz4D <- array(1:72,dim=c(4,3,2,3))

matriz4D


# < 054 >

# Funciones usuales:

# log exp sqrt abs sin cos tan asin acos atan

# gamma  lgamma floor ceiling trunc round


matriz

log(matriz)

sqrt(matriz)

matriz^2


# < 055 >

# Funciones vectoriales (cautela)

# min max range length sum prod 

# cumsum cumprod cummax cummin

# sort rank order rev

matriz

min(matriz)

max(matriz)

range(matriz)

length(matriz)

sum(matriz)

prod(matriz)


# < 056 >

matriz

cumsum(matriz)

cumprod(matriz)

cummax(matriz)

cummin(matriz) 


# < 057 >

matriz

rev(matriz)

rev(rev(matriz))

sort(matriz)


# < 058 >

# Operaciones usuales con matrices

# + t * %*% dim det solve eigen diag diag(diag(...)) outer

matriz

10+matriz

2*matriz

matriz+matriz

c(10,100,1000)+matriz

c(10,100,1000,10000)+matriz


# < 059 >

matriz

t(matriz)

matriz*matriz

matriz^2

matriz%*%matriz   # Error

matriz%*%t(matriz)

t(matriz)%*%matriz

t(matriz)*matriz  # Error


# < 060 >

dim(matriz)

dim(cubo)

dim(matriz4D)


# < 061 >

matrizC <- matrix(c(2,9,1,0,4,8,2,5,4,1,6,8),nrow=3,ncol=3)

matrizC

det(matrizC)

solve(matrizC)

matrizC%*%solve(matrizC)

round(matrizC%*%solve(matrizC),5)


# < 062 >

matrizC2 <- matrix(c(1,-1,0,-1,2,-1,0,-1,1),nrow=3,ncol=3)

matrizC2

det(matrizC2)

solve(matrizC2)

propios <- eigen(matrizC2)

propios

propios$values

propios$vectors

diag(matrizC2)

diag(diag(matrizC2))


# < 063 >

# Verificando diagonalizaci�n:

det(propios$vectors)

round(propios$vectors%*%(diag(propios$values)%*%solve(propios$vectors)),5)

matrizC2


# < 064 >

# Tablas de operadores binarios

# outer

x <- 1:4

y <- 10*(1:5)

x

y

mat1 <- outer(x,y,"+")

mat1

colnames(mat1) <- y

rownames(mat1) <- x

mat1


# < 065 >

mat2 <- outer(y,x,"/")

colnames(mat2) <- x

rownames(mat2) <- y

mat2


# < 066 >

x <- 1:5

y <- x

matH <- outer(x,y,function(i,j) 1/(i+j-1))

matH

matHinv <- solve(matH)

round(matH%*%matHinv,5)


# < 067 >

# Operaciones l�gicas con matrices (cautela)

matriz

matriz>3

1*(matriz>3)

which(matriz>5)

all(matriz>2)

any(matriz>2)

# Operaciones de conjuntos con matrices (cautela, an�logo a vectores)


# < 068 >

# Ordenando filas de matrices de acuerdo a una columna

(matriz <- matrix(runif(15),ncol=3))

matriz <- matrix(runif(15),ncol=3)

matriz

matriz[order(matriz[,1]),]

matriz[order(matriz[,2]),]

matriz[order(matriz[,3]),]


# < 069 > 

(matriz <- matrix(c(3,2,1,1,2,17,16,18,17,16,200,150,150,120,100),ncol=3))

matriz[order(matriz[,1]),]

matriz[order(matriz[,1],matriz[,2]),]

order(matriz[,1])

order(matriz[,1],matriz[,2])


