import math
import random
import pandas as pd
import numpy as np

class FuzzyCmeans:
    N = 0
    k = 0
    W = None 
    X = None

    def __init__(self,N,k,X):
        self.k = k
        self.N = N
        self.W = self.__iniciarW(N,k)
        self.saveW()
        self.X = X
        
    def saveW(self):
        with open('W.txt',"w") as archivo:
            for line in self.W:
                #print(str(line))
                archivo.write(str(line)+'\n')

    def __iniciarW(self,N,k):
        return np.random.rand(N,k)

    def distancia(self,punto1,punto2):
        d=0
        for i in range(len(punto1)):
            d=d+(punto1[i]-punto2[i])**2
        d=math.sqrt(d)
        return d

    def calcularCentro(self,c,m,X):
        num=0
        dem=0
        for i in range(0,self.N):
            num=num+(self.W[i][c]**m)*(X.iloc[i])
            #print(self.W[i][c])
            #print(m)
            #print(X.iloc[i])
            #print("Num: {}".format(num))
            #print("#"*30)
            dem=dem+(self.W[i][c]**m)
        center=num/dem
        return center

    def getDataW(self,i,j):
        print(self.W[i][j])

    def fit(self,upper_bound,m,X):
        r=0
        while r<upper_bound:
            c1 = self.calcularCentro(0,m,self.X)
            c2 = self.calcularCentro(1,m,self.X)
            c3 = self.calcularCentro(2,m,self.X)
            #print("C1: {}, C2: {}, C3: {}".format(c1,c2,c3))
            centers = np.concatenate((c1,c2,c3))
            centers=np.reshape(centers,(3,2))
            #print(centers.shape)
            #print(centers[0])
            suma = 0
            for i in range(N):
                for j in range(k):
                    print(self.distancia(X.iloc[i],centers[j]))
                    print(self.distancia(X.iloc[i],centers[0]))
                    if m==1:
                        pass
                    else:
                        print(2/(m-1))
                    
                    #suma += (/)**()
                    #print(suma)                       
            r=r+1


if __name__ == "__main__":
    #Paso 1 cargar los datos
    
    path = '/Users/drodriguez/Proyectos/Cinvestav_code/ciencia_datos/fuzzyCmeans/'
    data=pd.read_csv(path+'datos.csv',sep='\t', header=None)
    print("Data:")
    print(data.head())
    print("X:")
    X = data.iloc[:, 0:2]
    print(X.head())

    #Paso 3: Parametros
    k = 3
    upper_bound = 10 
    m = 0.5
    d = len(X.iloc[0])
    N = len(X.iloc[:,0])

    #data,k,upper_bound,m
    fcm=FuzzyCmeans(N,k,X)
    """
    c1 = fcm.calcularCentro(0,m,X)
    c2 = fcm.calcularCentro(1,m,X)
    c3 = fcm.calcularCentro(2,m,X)
    """
    fcm.fit(1,m,X)
    #fcm.fit(10)
