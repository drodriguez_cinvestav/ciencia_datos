import pandas as pd
import numpy as np
import math
import random

class Neurona:
    w=np.array([])
    n=0.01
    epocas=1

    def __init__(self,n,epocas):
        super().__init__()
        self.n=n
        self.epocas=epocas
    
    def fit(self, X_train, y_train):
        X_train.insert (0, "X0", 1)        
        num_variables=X_train.shape[1]
        self.iniciarW(num_variables)
        print("W0",self.w)
        
        e=0
        while(e<self.epocas):
            print("Epoca: ",e+1)
            #print("Wi:\t",self.w)
            deltaW=self.entrenar(X_train,y_train)
            #print("DeltaW:\t",deltaW)
            self.w+=deltaW
            e+=1
            #print("#"*40)

    def iniciarW(self, num_variables):
        for v in range(num_variables):
            #r=random.random()*10
            r=random.uniform(-1,1)
            self.w=np.append(self.w,r)

    def funcion_activacion(self,v):
        t=math.tanh(v)
        if t<=0:
            return 0
        else:
            return 1

    def entrenar(self, X_train, y_train):
        deltaW=np.array([])
        oD=self.genOD(X_train)
        
        #print("oD",oD)
        for col in X_train:
            #print("Col",col)
            #print("Columna:",X_train[col].values)
            
            newW=0
            acc_od=0   
            for indice_fila, fila in X_train.iterrows():
                #print("Indice fila: ",indice_fila)
                #print("fila.values",fila.values)
                #td=y_train[indice_fila]
                td=y_train[indice_fila]
                od=oD[acc_od]
                k=td-od
                #print("k:{},\t td:{},\t od:{}".format(k,td,od)) 
                #print("Valor",X_train[col].values[acc_od]) 
                newW+=k*X_train[col].values[acc_od]
                acc_od+=1
            #print("newW",newW)
            deltaW=np.append(deltaW,newW)
        return deltaW*self.n

    def genOD(self,X_train):
        od=np.array([])
        for indice_fila, fila in X_train.iterrows():
            #print(indice_fila)
            #print(fila.values)
            xd=np.array(fila.values) 
            odp = np.dot(xd,self.w)
            odp=self.funcion_activacion(odp)
            od=np.append(od,odp)
        return od

    def getW(self):
        return self.w

    def predict(self, x_test):
        x_test.insert (0, "X0", 1)
        predict=np.array([])
        for indice_fila, fila in x_test.iterrows():
            #print(indice_fila)
            #print(fila.values)
            a=np.asarray(fila)
            #print("fila:",a)
            p=self.funcion_activacion(np.dot(a,self.w))
            #print("Pedicto:",p)
            predict=np.append(predict,p)
        return predict
    
    def accuracy(self, predicted, original):
        lent = len(predicted)
        tp=0
        fn=0
        fp=0
        tn=0
        for i in range(lent):
            #print("Predicho:{},original:{}".format(predicted[i],original.iloc[i]))
            if(predicted[i] == 1 and original.iloc[i]==1):
                tp += 1
            elif(predicted[i] == 1 and original.iloc[i]==0):
                fp+=1
            elif(predicted[i] == 0 and original.iloc[i]==1):
                fn+=1
            elif(predicted[i] == 0 and original.iloc[i]==0):
                tn+=1
        
        tpfp=np.array([tp,fp])
        fntn=np.array([fn,tn])
        matrix=np.array([tpfp,fntn])
        
        precision=(tp/(tp+fp))*100
        recall=(tp/(tp+fn))*100
        accuracy=((tp+tn)/lent)*100
        print("Confusion Matrix: \n", matrix)
        print("Precision: {}, Recall: {}, Accuracy: {}".format(precision,recall,accuracy))
         