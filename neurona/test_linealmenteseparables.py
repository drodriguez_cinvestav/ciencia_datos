from Neurona import Neurona
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split

if __name__ == "__main__":
    #read data from .csv file
    path='/Users/drodriguez/proyectos/cinvestav/ciencia_datos/data/'
    data = pd.read_csv(path+"LinealmenteSeparable.csv",delimiter = ',')
    data.columns = ["X1", "X2", "C"]
    classes = data["C"]
    #print(data.head())
    data = data.drop(columns="C")
    x_train=data
    y_train=classes
    #print(y_train.head())
    
    #splitting test and train data for iris
    x_train, x_test, y_train, y_test = train_test_split(data, classes)
    #print(x_train.head())
    #print(y_train.head())
    
    #training the percpetron
    p = Neurona(0.01,15)
    p.fit(x_train, y_train)
    print("W",p.getW())
    pred = p.predict(x_test)
    #print("Predict",pred)
    p.accuracy(pred, y_test)
