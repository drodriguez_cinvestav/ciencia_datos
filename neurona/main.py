from Neurona import Neurona
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split

if __name__ == "__main__":
    #read data from .csv file
    path='/Users/drodriguez/proyectos/cinvestav/ciencia_datos/data'
    data = pd.read_csv(path+"/test_data.csv",delimiter = ',')
    data.columns = ["X1", "X2","X3", "C"]
    classes = data["C"]
    data = data.drop(columns="C")
    x_train=data
    y_train=classes
    #splitting test and train data for iris
    x_train, x_test, y_train, y_test = train_test_split(data, classes)
    #print(x_train.values)
    
    #training the percpetron
    p = Neurona(0.01,100)
    p.fit(x_train, y_train)
    print("weights: ", p.getW())
    pred = p.predict(x_test)
    p.accuracy(pred, y_test))
