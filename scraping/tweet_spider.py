import scrapy, time



class TweetSpider(scrapy.Spider):
	name = "tweet"
	allowed_domains = ["twitter.com"]
	start_urls = ["https://twitter.com/ExpPolitica"]
	  	
	def parse(self, response):
		tweets=response.css(r"span[class='css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0']")
		print("Resultados:", len(tweets))
		for href in tweets:
		#	url = response.urljoin(href)
		#	print(url)
		#	req = scrapy.Request(url, callback=self.parse_titles) #We have to create another method to parse films
		#	time.sleep(2)
			yield {}
	
	def parse_titles(self, response):
		data = {}
		for sel in response.css('html').extract():
			data['title'] = response.css(r"h1[id='firstHeading'] i::text").extract()
			data['director'] = response.css(r"tr:contains('Directed by') a[href*='/wiki/']::text").extract()
			data['starring'] = response.css(r"tr:contains('Starring') a[href*='/wiki/']::text").extract()
			data['releasedate'] = response.css(r"tr:contains('Release date') li::text").extract()
			data['runtime'] = response.css(r"tr:contains('Running time') td::text").extract()
		yield data
   