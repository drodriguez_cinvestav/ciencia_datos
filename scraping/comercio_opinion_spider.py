import scrapy, time

class ComercioOpinionSpider(scrapy.Spider):
	name = "peru"
	allowed_domains = ["elcomercio.pe"]
	start_urls = ["https://elcomercio.pe"]
	   	
	def parse(self, response):
		links=response.css(r"h5[itemprop='name'][class='separator__opinion-name mb-10'] a[href]::attr(href)").extract()
		print("Resultados:", len(links))
		for href in links:
			url = response.urljoin(href)
			print(url)
			#req = scrapy.Request(url, callback=self.parse_news) #We have to create another method to parse news
			#time.sleep(3)
			#yield req
	
	