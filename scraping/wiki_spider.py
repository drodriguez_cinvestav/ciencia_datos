import scrapy, time



class WikiSpider(scrapy.Spider):
	name = "wiki"
	allowed_domains = ["en.wikipedia.org"]
	start_urls = ["https://en.wikipedia.org/wiki/Academy_Award_for_Best_Picture"]
	  	
	def parse(self, response):
		links=response.css(r"tr[style='background:#FAEB86'] a[href*='film)']::attr(href)").extract()
		print("Resultados:", len(links))
		for href in links:
			url = response.urljoin(href)
			print(url)
			req = scrapy.Request(url, callback=self.parse_titles) #We have to create another method to parse films
			time.sleep(2)
			yield req
	
	def parse_titles(self, response):
		data = {}
		for sel in response.css('html').extract():
			data['title'] = response.css(r"h1[id='firstHeading'] i::text").extract()
			data['director'] = response.css(r"tr:contains('Directed by') a[href*='/wiki/']::text").extract()
			data['starring'] = response.css(r"tr:contains('Starring') a[href*='/wiki/']::text").extract()
			data['releasedate'] = response.css(r"tr:contains('Release date') li::text").extract()
			data['runtime'] = response.css(r"tr:contains('Running time') td::text").extract()
		yield data
   