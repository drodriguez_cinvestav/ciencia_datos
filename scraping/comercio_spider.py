import scrapy, time

class ComercioSpider(scrapy.Spider):
	name = "peru"
	allowed_domains = ["elcomercio.pe"]
	start_urls = ["https://elcomercio.pe"]
	   	
	def parse(self, response):
		links=response.css(r"h2[itemprop='name'] a[href]::attr(href)").extract()
		print("Resultados:", len(links))
		for href in links:
			url = response.urljoin(href)
			print(url)
			req = scrapy.Request(url, callback=self.parse_news) #We have to create another method to parse news
			time.sleep(3)
			yield req
	
	def parse_news(self, response):
		for sel in response.css('html').extract():
			data = {}
			data['title'] = response.css(r"h1[itemprop='name'][class='sht__title']::text").extract()
			data['summary'] = response.css(r"h2[itemprop='name'][class='sht__summary']::text").extract()
			
		yield data
   
   